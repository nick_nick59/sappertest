export const data = {
    table: [
        { Name: 'Mr. Timothy D. Cook', Title: 'CEO & Director', Pay: '11.56M', Born: '1961' },
        { Name: 'Ms. Katherine L. Adams', Title: 'Sr. VP, Gen. Counsel & Sec.', Pay: '3.6M', Born: '1964' }
    ],
    contacts: {
        caption: 'Apple, Inc.',
        address: 'One Apple Park Way Cupertino, CA 95014 United States',
        phone: '(408) 273 -34-43',
        site: 'https://www.apple.com'
    },
    info: {
        Sector: 'Technology',
        Industry: 'Consumer Electronics',
        Employees: '137’000'
    },
    text: 'Apple, Inc. engages in the design, manufacture, and sale of smartphones, personal computers, tablets, wearables and accessories, and other variety of related services. It operates through the following geographical segments: Americas, Europe, Greater China, Japan, and Rest of Asia Pacific. The Americas segment includes North and South America. The Europe segment consists of European countries, as well as India, the Middle East, and Africa. The Greater China segment comprises of China, Hong Kong, and Taiwan. The Rest of Asia Pacific segment includes Australia and Asian countries. Its products and services include iPhone, Mac, iPad, AirPods, Apple TV, Apple Watch, Beats products, Apple Care, iCloud, digital content stores, streaming, and licensing services. The company was founded by Steven Paul Jobs, Ronald Gerald Wayne, and Stephen G. Wozniak on April 1, 1976 and is headquartered in Cupertino, CA'
}

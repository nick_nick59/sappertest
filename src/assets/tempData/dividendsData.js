export const data = {
    table: [
        { Period: '3M', Ex_Date: 'February 6, 2020', Dividend: '0.77', Dividend_Yield: '0.24%' },
        { Period: '3M', Ex_Date: 'November 6, 2019', Dividend: '0.77', Dividend_Yield: '0.3%%' },
        { Period: '3M', Ex_Date: 'August 8, 2019', Dividend: '0.77', Dividend_Yield: '0.38%' },
        { Period: '3M', Ex_Date: 'May 9, 2019', Dividend: '0.77', Dividend_Yield: '0.41%' }
    ]
}
